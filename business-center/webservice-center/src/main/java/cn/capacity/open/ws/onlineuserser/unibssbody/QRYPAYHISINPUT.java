
package cn.capacity.open.ws.onlineuserser.unibssbody;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import cn.capacity.open.ws.onlineuserser.unibssbody.queryuserinforeq.QRYPAYHISREQ;
import cn.capacity.open.ws.unibssattached.UNIBSSATTACHED;
import cn.capacity.open.ws.unibsshead.UNIBSSHEAD;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ws.open.capacity.cn/unibssHead}UNI_BSS_HEAD"/>
 *         &lt;element name="UNI_BSS_BODY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://ws.open.capacity.cn/OnlineUserSer/unibssBody/queryUserInfoReq}QRY_PAY_HIS_REQ"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://ws.open.capacity.cn/unibssAttached}UNI_BSS_ATTACHED"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unibsshead",
    "unibssbody",
    "unibssattached"
})
@XmlRootElement(name = "QRY_PAY_HIS_INPUT")
public class QRYPAYHISINPUT {

    @XmlElement(name = "UNI_BSS_HEAD", namespace = "http://ws.open.capacity.cn/unibssHead", required = true)
    protected UNIBSSHEAD unibsshead;
    @XmlElement(name = "UNI_BSS_BODY", required = true)
    protected QRYPAYHISINPUT.UNIBSSBODY unibssbody;
    @XmlElement(name = "UNI_BSS_ATTACHED", namespace = "http://ws.open.capacity.cn/unibssAttached", required = true)
    protected UNIBSSATTACHED unibssattached;

    /**
     * Gets the value of the unibsshead property.
     * 
     * @return
     *     possible object is
     *     {@link UNIBSSHEAD }
     *     
     */
    public UNIBSSHEAD getUNIBSSHEAD() {
        return unibsshead;
    }

    /**
     * Sets the value of the unibsshead property.
     * 
     * @param value
     *     allowed object is
     *     {@link UNIBSSHEAD }
     *     
     */
    public void setUNIBSSHEAD(UNIBSSHEAD value) {
        this.unibsshead = value;
    }

    /**
     * Gets the value of the unibssbody property.
     * 
     * @return
     *     possible object is
     *     {@link QRYPAYHISINPUT.UNIBSSBODY }
     *     
     */
    public QRYPAYHISINPUT.UNIBSSBODY getUNIBSSBODY() {
        return unibssbody;
    }

    /**
     * Sets the value of the unibssbody property.
     * 
     * @param value
     *     allowed object is
     *     {@link QRYPAYHISINPUT.UNIBSSBODY }
     *     
     */
    public void setUNIBSSBODY(QRYPAYHISINPUT.UNIBSSBODY value) {
        this.unibssbody = value;
    }

    /**
     * Gets the value of the unibssattached property.
     * 
     * @return
     *     possible object is
     *     {@link UNIBSSATTACHED }
     *     
     */
    public UNIBSSATTACHED getUNIBSSATTACHED() {
        return unibssattached;
    }

    /**
     * Sets the value of the unibssattached property.
     * 
     * @param value
     *     allowed object is
     *     {@link UNIBSSATTACHED }
     *     
     */
    public void setUNIBSSATTACHED(UNIBSSATTACHED value) {
        this.unibssattached = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://ws.open.capacity.cn/OnlineUserSer/unibssBody/queryUserInfoReq}QRY_PAY_HIS_REQ"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "qrypayhisreq"
    })
    public static class UNIBSSBODY {

        @XmlElement(name = "QRY_PAY_HIS_REQ", namespace = "http://ws.open.capacity.cn/OnlineUserSer/unibssBody/queryUserInfoReq", required = true)
        protected QRYPAYHISREQ qrypayhisreq;

        /**
         * Gets the value of the qrypayhisreq property.
         * 
         * @return
         *     possible object is
         *     {@link QRYPAYHISREQ }
         *     
         */
        public QRYPAYHISREQ getQRYPAYHISREQ() {
            return qrypayhisreq;
        }

        /**
         * Sets the value of the qrypayhisreq property.
         * 
         * @param value
         *     allowed object is
         *     {@link QRYPAYHISREQ }
         *     
         */
        public void setQRYPAYHISREQ(QRYPAYHISREQ value) {
            this.qrypayhisreq = value;
        }

    }

}
