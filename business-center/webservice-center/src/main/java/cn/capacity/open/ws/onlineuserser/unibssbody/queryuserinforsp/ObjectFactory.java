
package cn.capacity.open.ws.onlineuserser.unibssbody.queryuserinforsp;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cn.capacity.open.ws.onlineuserser.unibssbody.queryuserinforsp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cn.capacity.open.ws.onlineuserser.unibssbody.queryuserinforsp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QRYPAYHISRSP.PARA }
     * 
     */
    public QRYPAYHISRSP.PARA createQRYPAYHISRSPPARA() {
        return new QRYPAYHISRSP.PARA();
    }

    /**
     * Create an instance of {@link QRYPAYHISRSP.PAYINFOLIST }
     * 
     */
    public QRYPAYHISRSP.PAYINFOLIST createQRYPAYHISRSPPAYINFOLIST() {
        return new QRYPAYHISRSP.PAYINFOLIST();
    }

    /**
     * Create an instance of {@link QRYPAYHISRSP }
     * 
     */
    public QRYPAYHISRSP createQRYPAYHISRSP() {
        return new QRYPAYHISRSP();
    }

}
